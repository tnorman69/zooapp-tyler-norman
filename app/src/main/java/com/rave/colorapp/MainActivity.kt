package com.rave.colorapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.rave.colorapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(R.layout.activity_main)
