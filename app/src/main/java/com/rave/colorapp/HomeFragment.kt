package com.rave.colorapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.rave.colorapp.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding: FragmentHomeBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() = with(binding) {
        gorillaBtn.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToAnimalFragment("Gorilla:", "Gorillas are apes")
            findNavController().navigate(action)
        }
        rhinoBtn.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToAnimalFragment("Rhino:","Rhinos have horny horns")
            findNavController().navigate(action)
        }
        tigerBtn.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToAnimalFragment("Tiger:","Joe Exotic is no longer the king of tigers")
            findNavController().navigate(action)
        }
        bearBtn.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToAnimalFragment("Grizzly Bear:","Grizzlies are sometimes mistaken for your mom...")
            findNavController().navigate(action)
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun String.toast() {
        Toast.makeText(activity, this, Toast.LENGTH_SHORT).show()
    }
}