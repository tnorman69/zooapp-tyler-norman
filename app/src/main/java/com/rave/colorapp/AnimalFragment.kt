package com.rave.colorapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.rave.colorapp.databinding.FragmentAnimalBinding


class AnimalFragment : Fragment() {
    private var _binding: FragmentAnimalBinding? = null
    private val binding: FragmentAnimalBinding get() = _binding!!

    private val args by navArgs<AnimalFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAnimalBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
        initViews()
    }

    private fun initViews() = with(binding) {
        tvFact.text = args.animalFact
        tvName.text = args.animalName
    }

    private fun initListeners() = with(binding) {
        rulesBackBtn.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun String.toast() {
        Toast.makeText(activity, this, Toast.LENGTH_SHORT).show()
    }
}