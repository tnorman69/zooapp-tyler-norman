package com.rave.colorapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.rave.colorapp.databinding.FragmentFullscreenBinding


class FullscreenFragment : Fragment() {
    private var _binding: FragmentFullscreenBinding? = null
    private val binding: FragmentFullscreenBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFullscreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() = with(binding) {
        helloFragNextBtn.setOnClickListener {
            val action = FullscreenFragmentDirections.actionFullscreenFragmentToBlueFragment()
            findNavController().navigate(action)
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun String.toast() {
        Toast.makeText(activity, this, Toast.LENGTH_SHORT).show()
    }
}